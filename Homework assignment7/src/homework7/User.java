package homework7;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class User {
	private int id;
	private String name;
	private java.time.LocalDate birthdate;

	public User(int id, String name, java.time.LocalDate birthdate) {
		this.id = id;
		this.name = name;
		this.birthdate = birthdate;
	}
	
	public java.time.LocalDate getDate() {
		return birthdate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public java.time.LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(java.time.LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public void sortListByType(int type) {

	}

	@Override
	public String toString() {
		return name + "'s ID number is " + id + ", their birthdate is " + birthdate;
	}
	
	public static void main(String[] args) {

		ArrayList<User> users = new ArrayList<User>();

		users.add(new User(1, "Steve", LocalDate.of(1990, 1, 5)));
		users.add(new User(2, "Kim", LocalDate.of(1991, 6, 3)));
		users.add(new User(3, "Paul", LocalDate.of(1993, 1, 25)));
		users.add(new User(4, "Jerry", LocalDate.of(1995, 2, 18)));
		users.add(new User(5, "Samantha", LocalDate.of(1990, 12, 12)));
		
		Collections.sort(users, (a, b) -> a.name.compareToIgnoreCase(b.name));
		System.out.println("Sorted by name");
		System.out.println(users);
		Collections.sort(users, (a, b) -> a.id < b.id ? -1: a.id == b.id ? 0 : 1);
		System.out.println("Sorted by ID number");
		System.out.println(users);
		Collections.sort(users, (a, b) -> a.getDate().compareTo(b.getDate()));
		System.out.println("Sorted by birthdate");
		System.out.println(users);
		
	}

}
