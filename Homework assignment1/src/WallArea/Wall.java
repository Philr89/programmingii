package WallArea;

public class Wall {
	
	double width;
	double height;
	
	public Wall() {
		
	}
	
	public Wall(double width, double height) {
		this.width = width;
		this.height = height;
		
		if (width < 0) {
			this.width = 0;
		}
		if (height < 0) {
			this.height = 0;
		}
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setWidth(double width) {
		this.width = width;
		
		if (width < 0) {
			this.width = 0;
		}
	}
	
	public void setHeight(double height) {
		this.height = height;
		
		if (height < 0) {
			this.height = 0;
		}
	}
	
	public double getArea() {
		return width * height;
	}

	public static void main(String[] args) {
		
		Wall wallTester = new Wall(5,4);
		System.out.println("area = " + wallTester.getArea());
		wallTester.setHeight(-1.5);
		System.out.println("width = " + wallTester.getWidth());
		System.out.println("height = " + wallTester.getHeight());
		System.out.println("area = " + wallTester.getArea());		

	}

}
