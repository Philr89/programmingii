package SumCalculator;

public class SimpleCalculator {
	
	double firstNumber = 0;
	double secondNumber = 0;
	
	public double getFirstNumber() {
		return firstNumber;
	}
	
	public double getSecondNumber() {
		return secondNumber;
	}
	
	public void setFirstNumber(double firstNumber) {
		this.firstNumber = firstNumber;
	}
	
	public void setSecondNumber(double secondNumber) {
		this.secondNumber = secondNumber;
	}
	
	public double getAdditionalResult() {
		return this.firstNumber + this.secondNumber;
	}
	
	public double getSubtractionResult() {
		//Exercise says to subtract value of second number from first
		//Output gives different result
		return this.secondNumber - this.firstNumber;
	}
	
	public double getMultiplicationResult() {
		return this.firstNumber * this.secondNumber;
	}
	
	public double getDivisionResult() {
		if (this.secondNumber == 0) {
			return 0;
		} else {
			return this.firstNumber / this.secondNumber;
		}
	}

	public static void main(String[] args) {
	
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4);
		System.out.println("add = " + calculator.getAdditionalResult());
		System.out.println("subtract = " + calculator.getSubtractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply = " + calculator.getMultiplicationResult());
		System.out.println("divide = " + calculator.getDivisionResult());
		
	}

}
