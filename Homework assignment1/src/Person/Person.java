package Person;

public class Person {
	
	String firstName = "";
	String lastName = "";
	int age;
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public int getAge( ) {
		return age;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setAge(int age) {
		this.age = age;
		if (age < 0 || age > 100) {
			age = 0;
		}
	}
	
	public boolean isTeen() {
		if (this.age > 12 && this.age < 20) {
			return true;
		}
		return false;
	}
	
	public String getFullName() {
		if (firstName == null && lastName == null) {
			return "";
		}else if (lastName == "") {
			return firstName;
		} else if (firstName == "") {
			return lastName;
		}else {
			return firstName + " " + lastName;
		}
	}	

	public static void main(String[] args) {
		
		Person personTester = new Person();
		personTester.setFirstName("");
		personTester.setLastName("");
		personTester.setAge(10);
		System.out.println("fullName = " + personTester.getFullName());
		System.out.println("teen = " + personTester.isTeen());
		personTester.setFirstName("John");
		personTester.setAge(18);
		System.out.println("fullName = " + personTester.getFullName());
		System.out.println("teen = " + personTester.isTeen());
		personTester.setLastName("Smith");
		System.out.println("fullName = " + personTester.getFullName());	

	}

}
