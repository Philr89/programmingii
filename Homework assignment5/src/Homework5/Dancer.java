package Homework5;

public class Dancer {

	protected int age;
	protected String name;

	public void dance() {

		this.age = 75;
		this.name = "Herbert";

		System.out.println(name + " is " + age + " years old and likes to dance the waltz");
		//added extra println so that the output is easier to read
		System.out.println();

	}

	public static void main(String[] args) {

		Dancer[] dancers = { new Dancer(), new ElectricBoogieDancer(), new Breakdancer() };

		for (int i = 0; i < dancers.length; i++) {

			dancers[i].dance();

		}

	}

}
