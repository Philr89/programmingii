package Homework6;

public class SalariedEmployee extends Employee {

	protected double basicSalary;

	public SalariedEmployee(String name, String ssn, double basicSalary) {
		super(name, ssn);
		this.basicSalary = basicSalary;
	}

	public double salary() {
		return this.basicSalary;
	}

	@Override
	public String toString() {
		return String.format("%s was paid %.2f",name, this.salary()); 
	}
	
	//You need to Override the toString method here because by default is returns a hashcode

}
