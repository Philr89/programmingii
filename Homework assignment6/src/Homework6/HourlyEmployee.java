package Homework6;

public class HourlyEmployee extends Employee {

	protected double wage;
	protected double hours;

	public HourlyEmployee(String name, String ssn, double wage, double hours) {
		super(name,ssn);
		this.wage = wage;
		this.hours = hours;
	}

	public double salary() {
		return wage * hours;
	}

	@Override
	public String toString() {
		return String.format("%s makes %.0f $ an hour and worked %.0f hours, they were paid %.2f",name, wage, hours, this.salary());
		//You need to Override the toString method here because by default is returns a hashcode
	}

}
