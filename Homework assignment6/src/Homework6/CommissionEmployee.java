package Homework6;

public class CommissionEmployee extends Employee {

	protected double sales;
	protected double commission;

	public CommissionEmployee(String name, String ssn, double sales, double commission) {
		super(name, ssn);
		this.sales = sales;
		this.commission = commission;
	}

	public double salary() {
		return sales * commission;
	}

	@Override
	public String toString() {
		return String.format("%s completed %.0f sales at %.0f commission, they were paid %.0f",name, sales, commission, this.salary());
		//You need to Override the toString method here because by default is returns a hashcode
	}

}
