package Homework6;

import java.util.Scanner;

public class Company {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("How many employees are you hiring?");
		int employeeCount = Integer.parseInt(scanner.nextLine());
		Employee[] employees = new Employee[employeeCount];
		
		for (int i = 0; i < employees.length; i++) {
			System.out.println("Enter the name of the employee");
			String name = scanner.nextLine();
			System.out.println("Enter " + name + "'s SSN");
			String ssn = scanner.nextLine();
			System.out.println("What type of Employee is " + name + ".  (Enter 1 for Commissioned, 2 for Hourly, 3 for Salaried");			
			int type = Integer.parseInt(scanner.nextLine());
			
			if (type == 1) {
				System.out.println("How many sales did " + name + " make?");
				int sales = Integer.parseInt(scanner.nextLine());
				
				System.out.println("What was " + name + "'s commission??");
				int commission = Integer.parseInt(scanner.nextLine());
				
				Employee commissionedEmployee = new CommissionEmployee(name, ssn, sales, commission);
				employees[i] = commissionedEmployee;
			} else if (type == 2) {
				System.out.println("What is " + name + "'s hourly wage?");
				int hourlyWage = Integer.parseInt(scanner.nextLine());
				
				System.out.println("How many hours did " + name + " work?");
				int hoursWorked = Integer.parseInt(scanner.nextLine());
				
				Employee hourlyEmployee = new HourlyEmployee(name, ssn, hourlyWage, hoursWorked);
				employees[i] = hourlyEmployee;
			} else if (type == 3) {
				System.out.println("What is " + name + "'s salary?");
				int salary = Integer.parseInt(scanner.nextLine());
				
				Employee salariedEmployee = new SalariedEmployee(name, ssn, salary);
				employees[i] = salariedEmployee;
			} else {
				System.out.println("ERROR:  Please select a type from 1 to 3");
				System.exit(0);
			}
			
		}
		
		Payroll payroll = new Payroll(employees);
		payroll.paySalary();
		
	}

}
