package Homework6;

public abstract class Employee {

	protected String name;
	protected String ssn;
	
	public Employee(String name, String ssn) {
		this.name = name;
		this.ssn = ssn;
	}

	public abstract double salary();

	public abstract String toString();

}
