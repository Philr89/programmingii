package AnimalExersize;

class Cats extends Animals{
	public void sound() {
		System.out.println("Meow");
	}
}

class Dog extends Animals {
	public void sound() {
		System.out.println("Woof");
	}
}

abstract class Animals {

	abstract void sound();

}

public class AnimalExercise {	

	public static void main(String[] args) {

		// Trying to understand why I cannot instantiate Cats (tried static)
		// TODO figure out why this doesn't work
		Cats cat = new Cats();
		Dog dog = new Dog();

		cat.sound();
		dog.sound();
	}

}
