package Perimeter;

public class PerimeterWithoutAbstraction {
		
		public void perimeter(int numberOfSides, double lengthOfSide) {
			
			//This method would not work for a circle, we would need to write a separate one
			
			double perimeter = numberOfSides * lengthOfSide;
						
			if (numberOfSides <= 2) {
				System.out.println("Error: a shape must have at least three sides");
			} else {
				System.out.println("The perimeter of this shape is " + perimeter);
			}
		}
		
		public void area(double width, double height) {
			System.out.println("The area of this shape is " + (width * height) + "m");
		}

	public static void main(String[] args) {
		
		PerimeterWithoutAbstraction shapeCalculator = new PerimeterWithoutAbstraction();
		shapeCalculator.perimeter(3,5);
		shapeCalculator.area(4, 8);
		

	}

}
