package Perimeter;

public abstract class PerimeterWithAbstraction {

	abstract double getPerimeter(double length);

	abstract double getArea(double length);

}

class Square extends PerimeterWithAbstraction {

	public double getPerimeter(double length) {
		return length * 4;
	}

	public double getArea(double length) {
		return length * length;
	}
}

class Triangle extends PerimeterWithAbstraction {

	public double getPerimeter(double length) {
		return length * 3;
	}

	public double getArea(double length) {
		return length * length;

	}
}

class Circle extends PerimeterWithAbstraction {

	public double getPerimeter(double length) {
		double radius = length / 2;
		return Math.PI * 2 * radius;
	}

	public double getArea(double length) {
		double radius = length / 2;
		return Math.PI * (radius * radius);
	}

	public static void main(String[] args) {

		Square shape1 = new Square();
		Triangle shape2 = new Triangle();
		Circle shape3 = new Circle();

		System.out.println("Area of a square is " + shape1.getArea(5));
		System.out.println("Area of a triangle is " + shape2.getArea(8));
		System.out.println("Area of a circle is " + shape3.getArea(8));
		System.out.println("Perimeter of a square is " + shape1.getPerimeter(5));
		System.out.println("Perimeter of a triangle is " + shape2.getPerimeter(8));
		System.out.println("Circumference of a circle is " + shape3.getPerimeter(8));

	}

}
//We can use an abstract class here to outline the two values we are looking for (area and perimeter) for all of our shapes.
//By creating the abstract class and methods, I can use it and customize it for each shape that we want to find out