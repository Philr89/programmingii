package Calculations;

abstract class Marks {

	abstract void getPercentage();
}

class StudentA extends Marks {

	int subject1;
	int subject2;
	int subject3;

	// Class solution
	// private int subject1;
	// private int subject2;
	// private int subject3;

	public StudentA(int subject1, int subject2, int subject3) {

		// this.subject1 = subject1;
		// this.subject1 = subject2;
		// this.subject1 = subject3;

		if (subject1 > 100 || subject2 > 100 || subject3 > 100) {
			System.out.println("Error: mark must be between 0 and 100");
		}

	}

	public void getPercentage() {
		int result = (subject1 + subject2 + subject3) / 3;
		System.out.println("The percent for student A is " + result);
	}

}

class StudentB extends Marks {

	int subject1;
	int subject2;
	int subject3;
	int subject4;

	public StudentB(int subject1, int subject2, int subject3, int subject4) {

		if (subject1 > 100 || subject2 > 100 || subject3 > 100 || subject4 > 100) {
			System.out.println("Error: mark must be between 0 and 100");
		}

	}

	public void getPercentage() {
		int result = (subject1 + subject2 + subject3 + subject4) / 4;
		System.out.println("The percent for student B is " + result);

	}

}

public class Grades {
	
	public static void main(String[] args) {

		// Why does this not work??
		// Ask in class tomorrow
		StudentA studentA = new StudentA(100,50,74);
		StudentB studentB = new StudentB(88,94,99,79);
		
		studentA.getPercentage();
		studentB.getPercentage();

	}

}
