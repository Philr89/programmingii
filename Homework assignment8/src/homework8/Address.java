package homework8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Address {
	private int buildingNo;
	private String streetName;
	private String cityName;
	private String provinceName;
	private String postalCode;

	public Address(int buildingNo, String streetName, String cityName, String provinceName, String postalCode) {
		super();
		this.buildingNo = buildingNo;
		this.streetName = streetName;
		this.cityName = cityName;
		this.provinceName = provinceName;
		this.postalCode = postalCode;
	}

	public int getBuildingNo() {
		return buildingNo;
	}

	public void setBuildingNo(int buildingNo) {
		this.buildingNo = buildingNo;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return String.format("%s %s %s %s, %s", buildingNo, streetName, cityName, provinceName, postalCode);
	}

	public static void writeToFile(ArrayList<Address> list) throws IOException {
		FileWriter fileWriter = new FileWriter("resource/file.txt");
		for (Address address : list) {
			fileWriter.write(address.toString() + "\n");
		}
		fileWriter.close();
	}

	public static void readFromFile() throws Exception {
		FileReader reader = new FileReader("resource/file.txt");
		BufferedReader buffer = new BufferedReader(reader);
		String line = buffer.readLine();
		do {			
			System.out.println(line);
		}
		while ((line = buffer.readLine()) != null); {
			System.out.println(line);
		}
		buffer.close();
	}

	public static void main(String[] args) throws Exception {

		ArrayList<Address> addressBook = new ArrayList();

		addressBook.add(new Address(5, "Morris Av.", "Louisville", "NB", "1K2 9S5"));
		addressBook.add(new Address(23, "Joust blvd.", "Louisville", "NB", "3N2 9SK"));
		addressBook.add(new Address(66, "Horse ln.", "Louisville", "NB", "8SJ 3K2"));
		addressBook.add(new Address(4554, "Horse ln.", "Louisville", "NB", "2M2 0S2"));
		addressBook.add(new Address(234, "Fake street.", "Falsetown", "SF", "0M2 2K2"));

		writeToFile(addressBook);

		readFromFile();

	}

}
